    <style type="text/css">
        .collapseProv{
            color: #000 !important;
            height: 25px !important;
        }
    </style>

    <!--::blog_part start::-->
    <section class="blog_part section_padding">
        <div class="container">


            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="section_tittle text-center">
                        <h2>INDICADORES DE SALUD EN EL NEA</h2>
                    </div>
                </div>
            </div>



            <div class="row">

                    <div class="col-sm-8 col-lg-3 col-xl-3">
                        <div class="single-home-blog">
                            <div class="card">
                                <img src="img/maps/mapa_chaco.png" class="card-img-top" alt="blog">
                                 <div class="card-body">


                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="col-sm-6 col-lg-3 col-xl-3">
                        <div class="single-home-blog">
                            <div class="card">
                                <img src="img/maps/mapa_corrientes.png" class="card-img-top" alt="blog">
                                <div class="card-body">


                                    <a href="#AnclaProvincias" data-toggle="collapse" class="collapseProv" 
                                        onclick="ShowHideMenus('corrientes','situacion_sociodemografica/estructura_de_la_poblacion');">
                                        SITUACION SOCIODEMOGRAFICA</a>


                                    <br>
                                    <a data-toggle="collapse" href="#ctes-situcion-salud" class="collapseProv">SITUACION DE SALUD</a>
                                    <div id="ctes-situcion-salud" class="panel-collapse collapse">
                                        <a href="#AnclaProvincias" class="collapseProv" 
                                            onclick="ShowHideMenus('corrientes','situacion_de_salud/ENFERMEDADES_DE_TRANSMISION_VECTORIAL');">
                                            &nbsp;&nbsp;&#8226; Enfermedades de Transmisión Vectorial </a><br>
                                        <a href="#AnclaProvincias" class="collapseProv" 
                                            onclick="ShowHideMenus('corrientes','situacion_de_salud/MUERTE_Y_ENFERMEDADES');">
                                            &nbsp;&nbsp;&#8226; Muerte y Enfermedades </a><br>
                                        <a href="#AnclaProvincias" class="collapseProv" 
                                            onclick="ShowHideMenus('corrientes','situacion_de_salud/SALUD_ADOLESCENTE');">
                                            &nbsp;&nbsp;&#8226; Salúd Adolescente </a><br>
                                        <a href="#AnclaProvincias" class="collapseProv" 
                                            onclick="ShowHideMenus('corrientes','situacion_de_salud/SALUD_MATERNO_INFANTIL');">
                                            &nbsp;&nbsp;&#8226; Salúd Materno Infantil </a><br>                                            
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>










                    <div class="col-sm-8 col-lg-3 col-xl-3">
                        <div class="single-home-blog">
                            <div class="card">
                                <img src="img/maps/mapa_formosa.png" class="card-img-top" alt="blog">
                                <div class="card-body">


                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-sm-6 col-lg-3 col-xl-3">
                        <div class="single-home-blog">
                            <div class="card">
                                <img src="img/maps/mapa_misiones.png" class="card-img-top" alt="blog">
                                <div class="card-body">

                                
                                </div>
                            </div>
                        </div>
                    </div>





        </div>
    

    <div id="AnclaProvincias"></div>
    </section>
    <!--::blog_part end::-->





<script type="text/javascript">
    function ShowHideMenus(provincia,ruta){
        variable = $('#MenuProvincias').hide(300);
        variable = $('#MenuProvincias').show(300);

        $.ajax({
          url: 'menus_provincias.php',
          type: 'GET',
          data: 'provincia='+provincia+'&ruta='+ruta,
          success: function(data) {
            //called when successful
            //$('#ajaxphp-results').html(data);
            $('#MenuProvinciasContenido').html(data);
            
          },
          error: function(e) {

            //called when there is an error
            //console.log(e.message);
          }
        });

    }


</script>


<div id="MenuProvincias" style="display: none; padding-top: 107px; margin-top: -100px;">
    <div id="MenuProvinciasContenido"></div>
</div>    