    <script type="text/javascript">
        function ShowHide(elemento){
            variable = $(elemento).find('p').toggle(300);
        }
    </script>

    <!-- feature_part start-->
    <section class="feature_part">
        <div class="container">
            <div class="row">
                <div class="col-xl-24 col-md-24 align-self-center">
                    <div class="single_feature_text ">
                        <h2>TEMAS GLOBALES</h2>
                        <p>Los TEMAS GLOBALES: representan una abstracción que singulariza la realidad compleja en los diferentes Temas que, de un modo u otro, se vinculan con la Salud Pública de la Provincia. Cada uno de ellos engloba diversas unidades temáticas que dimensionan aspectos del tema en cuestión.</p>
                        <br>
                        <br>
                        <!--a href="#" class="btn_2">More service</a-->
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class="feature_item">
                        <div class="row">




                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Determinantes Socio Ambientales</h4>
                                        <br>
                                        <p style="display: none;">La información en el tópico Determinantes Socioambientales se organiza en Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Financiamiento en Salud</h4>
                                        <p style="display: none;">La información en el tópico Financiamiento se organiza en Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Infraestructura y Recursos en Salud</h4>
                                        <p style="display: none;">La información en el tópico Infraestructura y Recursos se organiza en Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Políticas Formación y Capacitacion <br>de Recursos Humanos</h4>
                                        <p style="display: none;">La información en el tópico Políticas de Recursos Humanos se organiza en Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>







                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Políticas Sociosanitarias</h4>
                                        <p style="display: none;">La información en el tópico Políticas sociosanitarias se organiza Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Situación Demográfica</h4>
                                        <p style="display: none;">La información en el tópico Situación Demográfica se organiza en Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Situación de Salud</h4>
                                        <p style="display: none;">La información en el tópico Situación de Salud se organiza en Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-lg-6 col-sm-6 pointer" onclick="ShowHide(this);">
                                <div class="single_feature">
                                    <div class="single_feature_part">
                                        <span class="single_feature_icon"><img src="img/mini_obs_byn_brain.png" style="width: 45px;"></span>
                                        <h4>Situación Socioeconómica</h4>
                                        <p style="display: none;">La información en el tópico Situación Socioeconómica se organiza en Unidades Temáticas. Por lo cual, las Unidades Temáticas que se presentan a continuación, son dimensiones a través de las cuales se conforma el Tema Global de la referencia. Representan una desagregación que hace posible el puente entre los grandes temas y la realidad que se pretende analizar. Cada una de ellas contiene variables e indicadores que operacionalizan la temática, de manera que pueda ser aprehendida de un modo preciso y objetivo.</p>
                                    </div>
                                </div>
                            </div>  
                            





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature_part start-->